extends RichTextLabel

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

var dialog = []
var page = 0

func _ready():
	set_process_input(true)

func _initial_set():
	set_bbcode(dialog[page])
	set_visible_characters(0)


func _process(delta):
	if Input.is_action_just_pressed("ui_select"):
		if get_visible_characters() > get_total_character_count():
			if page < dialog.size()-1:
				page += 1
				set_bbcode(dialog[page])
				set_visible_characters(0)
			else:
				get_tree().change_scene("res://TitleScreen/TitleScreen.tscn")
		else:
			set_visible_characters(get_total_character_count())

#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func _on_Timer_timeout():
	set_visible_characters(get_visible_characters()+1)
	pass # replace with function body
