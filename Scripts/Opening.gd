extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():

	var DialogNode = get_node("Opening Text")
	
	DialogNode.dialog.append("After 10 years of being stuck in the depth of the Alcatraz prison, Mr. Nobody finally decided that he's had enough and would like to escape from the confines of his lonely cell. Our mission is to help Mr. Nobody in his attempt to escape the high security prison by sneaking past the guards.")
	DialogNode._initial_set()
