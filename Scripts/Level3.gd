extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	var success = AudioStreamPlayer.new()
	success.stream = load("res://Songs/346116__lulyc__retro-game-heal-sound.wav")
	self.add_child(success)
	success.play()

	var DialogNode = get_node("Dialog Box/DialogBox/RichTextLabel")
	var PlayerNode = get_node("Player")
	
	DialogNode.dialog.append("Halfway through...")
	DialogNode._initial_set()
