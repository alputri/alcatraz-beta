extends KinematicBody2D

const TYPE = "ENEMY"
const DAMAGE = 1

export var speed = 100
var bounce_coefficient = 1.0
var reflect = true
var velocity = Vector2(0,0)
onready var initialEnemyPos = get_position()

func damage_loop():
	for body in $hitbox.get_overlapping_bodies():
		if body.get("DAMAGE") == 2 and body.get("TYPE") != TYPE:
			set_position(initialEnemyPos)

func _physics_process(delta):
	var collision = move_and_collide(velocity * delta)
	if collision:
		var motion = collision.remainder.bounce(collision.normal)
		velocity = velocity.bounce(collision.normal)
		move_and_collide(motion)
	damage_loop()