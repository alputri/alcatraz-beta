extends Node2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():

	var DialogNode = get_node("Opening Text")
	
	DialogNode.dialog.append("At long last, after some close calls and near deaths, Mr. Nobody finally escaped the tightly-guarded Alcatraz Prison. The feeling of being reunited with his family has never been this sweet. But one final question remains, is he actually safe now?\n\nWell,\nNo one knows.")
	DialogNode._initial_set()
